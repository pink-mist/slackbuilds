Sat Jul  2 14:27:42 UTC 2016
development/lazarus: Fix MD5SUM.
network/toxcore: Update DEP.
+--------------------------+
Sat Jul  2 00:09:35 UTC 2016
This is not a drill. Slackware 14.2 has been released.
  Here it is! The launch of the SlackBuilds.org repository into the 14.2 era.
  With the help of our amazing community we spent the last couple of months
  testing, fixing, and updating the scripts in the repository for the latest
  Slackware release.  We are happy to say that we think it has never been
  better (yes, we say this every time)  :-)
We've added a new template script (haskell) and made some minor tweaks to
  the existing templates, so be sure to check those out before starting to
  submit new scripts.
We'd like to take this time to welcome the newest member of our admin team,
  David Spencer (idlemoor) - it seems that we have one new member during 
  each release cycle, so start making your bids now ;-)
Thanks to all of our users and maintainers for continuing to do what you
  do - your dedication and attention to detail is greatly appreciated.
We've recently added our repos on gitlab to provide another way, perhaps
  easier for some, to accept changes and new submissions - we're not exactly
  sure how it will go, but we're going to give it a try anyway. Merge requests
  are welcome: https://gitlab.com/groups/SlackBuilds.org
Enjoy!
+--------------------------+
